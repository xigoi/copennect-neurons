import ../levels, ../neurons

let
  n0 = Neuron(number: 1)
  n1 = Neuron(number: 1, connected: @[n0])
  n2 = Neuron(number: 2, connected: @[n0, n1])
  l = Level(neurons: @[n0, n1, n2])
  c = ClassicLevel(neurons: @[n2, n1, n0, Neuron(number: 0)], width: 2)
doAssert $l == "1 1 2"
doAssert $c == "2-1\n|/ \n1 0"
