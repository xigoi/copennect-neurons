import sequtils, strutils
import neurons

type
  Level* = ref object of RootObj
    neurons*: seq[Neuron]
  ClassicLevel* = ref object of Level
    width*: Positive
  RectangularLevel* = ClassicLevel

proc `[]`*(level: Level, index: Natural): Neuron =
  level.neurons[index]

method `$`*(level: Level): string {.base.} =
  level.neurons.mapIt($it).join(" ")

proc `[]`*(level: RectangularLevel, y: Natural, x: Natural): Neuron =
  level.neurons[y * level.width + x]

proc height*(level: RectangularLevel): Natural =
  level.neurons.len div level.width

method `$`*(level: ClassicLevel): string =
  for y in 0..<level.height:
    if y != 0:
      result.add "\n"
      for x in 0..<level.width:
        if x != 0:
          let
            slash = level[y, x-1] in level[y-1, x].connected
            backslash = level[y, x] in level[y-1, x-1].connected
          if slash:
            if backslash:
              result.add "X"
            else:
              result.add "/"
          else:
            if backslash:
              result.add "\\"
            else:
              result.add " "
        if level[y, x] in level[y-1, x].connected:
          result.add "|"
        else:
          result.add " "
      result.add "\n"
    for x in 0..<level.width:
      if x != 0:
        if level[y, x] in level[y, x-1].connected:
          result.add "-"
        else:
          result.add " "
      result.add $level[y, x]
