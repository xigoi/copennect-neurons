type
  Neuron* = ref object of RootObj
    number*: Natural
    adjacent*: seq[Neuron]
    connected*: seq[Neuron]

proc init(neuron: Neuron, number: Natural = 0) =
  neuron.number = number

proc newNeuron*(number: Natural = 0): Neuron =
  new result
  result.init(number)

method `$`*(neuron: Neuron): string {.base.} =
  $neuron.number
